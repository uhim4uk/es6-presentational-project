/**
 * !Block scoped variables #1
 */

// console.log(varNum);

// var varNum = 1;
// let varNum = 1;
// const varNum = 1;

// varNum += 1;

// console.log(varNum);

/**
 * !Block scoped variables #2
 */
// for (var i = 0; i < 5; i++) {
//   var btn = document.createElement('button');
//   btn.appendChild(document.createTextNode('Button ' + i));

//   btn.addEventListener('click', function() {
//     console.log(i);
//   });

//   document.body.appendChild(btn);
// }

// for (let i = 0; i < 5; i++) {
//   var btn = document.createElement('button');
//   btn.appendChild(document.createTextNode('Button ' + i));

//   btn.addEventListener('click', function() {
//     console.log(i);
//   });

//   document.body.appendChild(btn);
// }

/**
 * ! Destructuring & Spread
 */
// const entry = {
//   target: 'This is the fancy target',
//   isIntersecting: true
// };

// const arrEntry = [1, 2, 3, 4];
// const arrEntry2 = [5, 6, 7, 8];

// !object destructuring
// const { target, isIntersecting } = entry;

// console.log(target, isIntersecting);
// console.log(entry.target, entry.isIntersecting);

// function destruct({ target, isIntersecting }) {
//   console.log(target, isIntersecting);
// };

// destruct(entry);

// const { target: notATarget, isIntersecting: is } = entry;
// console.log(target, isIntersecting);
// console.log(notATarget, is);

// !object spread
// const newEntry = {
//   target1: 'Ooops missed that',
//   isIntersecting1: false
// };

// console.log({ ...entry, ...newEntry });
// console.log({ ...entry, ...newEntry, newValue: 'On the fly, baby' });

// !Array destructuring
// const [first, second] = arrEntry;
// console.log(first, second);

// const [first, second, ...third] = arrEntry;
// console.log(first, second, third);

// !Array spread
// console.log([...arrEntry, ...arrEntry2]);
// console.log([...arrEntry, ...arrEntry2, 'whaaat?']);
// console.log(['hey!', ...arrEntry, ...arrEntry2]);

/**
 * ! Object/Array mutation
 */

// const entry = {
//   target: 'This is the fancy target',
//   isIntersecting: true,
//   hiddenField: {
//     value: '******'
//   }
// };
// const newEntry = entry;

// newEntry.target = 'Whoops, target lost!';
// console.log('New: ', newEntry.target);
// console.log('Old: ', entry.target);

// const entry = {
//   target: 'This is the fancy target',
//   isIntersecting: true
// };

// const newEntry = { ...entry };

// newEntry.target = 'Whoops, target lost!';
// console.log('New: ', newEntry.target);
// console.log('Old: ', entry.target);

// const newEntry = { ...entry };
// newEntry.hiddenField.value = 'NOT HIDDEN';
// console.log('New: ', newEntry.hiddenField.value);
// console.log('Old: ', entry.hiddenField.value);

/**
 * ! Promises
 */

// var _promise = new Promise(function(resolve, reject) {
//   setTimeout(function() {
//     const randomNumber = Math.random();

//     if (randomNumber > 0.5) {
//       resolve(`Resolved with value: ${randomNumber}`);
//     } else {
//       reject(`Rejected with value: ${randomNumber}`);
//     }
//   }, 2000);
// });

// _promise
//   .then(function(value) {
//     console.log(value);
//   })
//   .catch(function(val) {
//     console.error(val);
//   });

// console.log(_promise);

// !Async/Await

// async function asyncFunc() {
//   const returnedValue = await _promise;
//   console.log(returnedValue);
// }

// asyncFunc();

// async function asyncFunc() {
//   try {
//     const returnedValue = await _promise;
//     console.log(returnedValue);
//   } catch (error) {
//     console.error(error);
//   }

//   // console.log('After resolve');
// }

// asyncFunc();
// console.log('After async function call');
