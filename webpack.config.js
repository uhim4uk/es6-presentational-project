'use strict';
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');

module.exports = {
  context: __dirname,
  entry: {
    scripts: './js/scripts.js',
    'style.css': './scss/style.scss'
  },
  output: {
    path: path.join(__dirname, '/public/'),
    filename: '[name].min.js',
    publicPath: '/public/'
  },
  devServer: {
    publicPath: '/public/',
    port: 8080
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  '@babel/preset-env',
                  {
                    targets: 'last 1 firefox version'
                  }
                ]
              ]
            }
          }
        ]
      },
      {
        test: /\.(png)$/,
        use: [
          {
            loader: 'file-loader'
            //  'file?name=dist/fonts/[name].[ext]'
          }
        ]
      },
      {
        test: /\.(css|sass|scss)$/,
        use: ExtractTextPlugin.extract({
          use: [
            {
              loader: 'css-loader',
              options: {
                minimize: true
              }
            },
            'postcss-loader',
            'sass-loader'
          ]
        })
      }
    ]
  },
  mode: 'development',
  plugins: [
    // new webpack.optimize.ModuleConcatenationPlugin(),
    new ExtractTextPlugin('style.css')
    // new BrowserSyncPlugin(
    //   {
    //     host: 'localhost',
    //     port: 3006,
    //     files: ['public/style.css', 'public/scripts.min.js'],
    //     open: false,
    //     proxy: 'http://archery-poltava.local/'
    //   },
    //   {
    //     reload: false
    //   }
    // )
  ]
};
