import { fetchAndResolve, apiRequest } from './api';

const hidePreloader = () => {
  const preloader = document.querySelector('.filter-preloader');
  preloader.classList.remove('loading');
};

const showPreloader = () => {
  const preloader = document.querySelector('.filter-preloader');
  preloader.classList.add('loading');
};

/**
 * Creates the post HTML and returns it as a string
 * @param {string} res Resulting HTML string of all the posts
 * @param {Object} post Post object
 */
const createPostString = (res, post) => {
  if (!post || !post.hasOwnProperty('url')) return res;

  const { url, title, time, score, by: author } = post;
  const d = new Date(time * 1000);
  const addZero = num => (num < 10 ? `0${num}` : num);
  const fullDate = `${addZero(d.getHours())}:${addZero(
    d.getMinutes()
  )} ${d.getDate()}-${d.getMonth()}-${d.getFullYear()}`;

  return (
    res +
    `
  <a href=${url} class="post">
    <div class="post-data">
      <span class="date">${fullDate}</span>
      <span class="score">${score}</span>
    </div>
    <div class="post-content">
      <h3>${title}</h3>
      <p>by: <b>${author}</b></p>
    </div>
  </a>
  `
  );
};

/**
 * Renders posts from Hacker News API data
 * @param {Object[]} posts Array of post objects from Hacker News Api
 */
const renderPosts = posts => {
  if (!posts && !posts.length) return;

  const postsContainer = document.querySelector('.filtering-content');
  const postsString = posts.reduce(createPostString, '');

  postsContainer.innerHTML = postsString;
  hidePreloader();
};

/**
 *
 * @param {Object[]} posts Array of post objects from Hacker News Api
 * @param {Element} sortByNode Select which is responsible for sorting data type
 * @param {Element} sortOrderNode Select which is responsible for filtering order
 * @returns {function():void} Function to trigger filtering process
 */
const filterPosts = (posts, sortByNode, sortOrderNode) => () => {
  const newPosts = posts.map(post => ({ ...post }));
  const sortBy = sortByNode.value;
  const isAsc = sortOrderNode.value === 'asc';

  const sortingFunc = (a, b) => {
    return isAsc ? a[sortBy] - b[sortBy] : b[sortBy] - a[sortBy];
  };

  renderPosts(newPosts.sort(sortingFunc));
};

/**
 * Initializes filtering functionality and renders posts list
 * @param {Object[]} posts Array of post objects from Hacker News Api
 */
const initFiltering = posts => {
  // console.log(posts);
  if (!posts && !posts.length) return;
  const sortBy = document.querySelector('.sort-by');
  const sortOrder = document.querySelector('.sort-order');
  const filterWithData = filterPosts(posts, sortBy, sortOrder);

  filterWithData();

  sortBy.addEventListener('change', filterWithData);
  sortOrder.addEventListener('change', filterWithData);
};

/**
 * Fetches latest 20 posts
 * @param {number[]} latestStories Latest posts ids
 */
const fetchLatestPosts = latestStories => {
  if (!latestStories && !latestStories.length)
    throw new Error('Something went wrong on api server!');

  const promiseArray = [];

  for (let i = 0; i < 20; i++) {
    promiseArray.push(apiRequest(`item/${latestStories[i]}`));
  }

  Promise.all(promiseArray)
    .then(responses => {
      const results = responses.map(res => res.json());
      return Promise.all(results);
    })
    .then(posts => {
      initFiltering(posts);
    })
    .catch(error => {
      console.error(error);
    });
};

/**
 * Init news block functionality and fetch latest post id form Hacker News API
 */
const initNews = () => {
  const refreshBtn = document.querySelector('.refresh-button');

  fetchAndResolve('newstories', fetchLatestPosts);

  refreshBtn.addEventListener('click', () => {
    showPreloader();
    fetchAndResolve('newstories', fetchLatestPosts);
  });
};

export default initNews;
