// import headerScripts from './header.js';
import headerScripts from './header-wrong.js';
import sliderScripts from './slider.js';
import lazyLoad from './lazyLoad.js';
import mapScripts from './map.js';
import newsScripts from './news.js';
import splitScripts from './splitAnimation';

lazyLoad();
headerScripts();
sliderScripts();
mapScripts();
newsScripts();
splitScripts();
