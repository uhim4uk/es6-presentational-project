import { tns } from 'tiny-slider';
/**
 * Init tiny-slider script
 */
const sliderInit = () => {
  const sliderContainer = document.querySelector('.slider-wrapper');

  if (!sliderContainer) return;

  tns({
    container: sliderContainer,
    autoplay: true,
    lazyload: true,
    arrowKeys: true,
    mouseDrag: true,
    nav: false,
    autoplayButton: false,
    autoplayButtonOutput: false,
    controlsText: ['<span><</span>', '<span>></span>']
  });
};

export default sliderInit;
