import L from 'leaflet';
import { GestureHandling } from 'leaflet-gesture-handling';

/**
 * Creates and appends to the DOM link to map library css
 */
const downloadMapStyles = () => {
  const styleUrls = [
    'https://unpkg.com/leaflet-gesture-handling/dist/leaflet-gesture-handling.min.css',
    'https://unpkg.com/leaflet@1.3.3/dist/leaflet.css'
  ];

  const fragment = document.createDocumentFragment();

  styleUrls.forEach(url => {
    const link = document.createElement('link');
    link.href = url;
    link.rel = 'stylesheet';
    fragment.appendChild(link);
  });

  document.body.appendChild(fragment);
};

/**
 * Initializes Leaflet map on the page
 */
const initMap = () => {
  const map = document.querySelector('.map-wrapper');

  if (!map) return;

  downloadMapStyles();

  L.Map.addInitHook('addHandler', 'gestureHandling', GestureHandling);

  const mapObj = L.map(map, {
    gestureHandling: true
  }).setView([49.5929159, 34.5480751], 16);

  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution:
      '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  }).addTo(mapObj);

  const customIcon = L.icon({
    iconUrl: './images/logo.png',
    iconSize: [30, 30], // size of the icon
    iconAnchor: [15, 15] // point of the icon which will correspond to marker's location
  });

  const marker = L.marker([49.5929159, 34.5480751], { icon: customIcon }).addTo(
    mapObj
  );
};

export default initMap;
