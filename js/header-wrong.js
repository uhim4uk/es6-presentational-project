/**
 * @param {Element} link One of header links
 * @returns {(e: Object) => void} callback for the event listener
 */
const bindScrollToLink = e => {
  e.preventDefault();
  const scrollToBlock = document.querySelector(e.target.hash) || {
    offsetTop: 0
  };

  window.scroll({
    top: scrollToBlock.offsetTop,
    left: 0,
    behavior: 'smooth'
  });
};

/**
 * Handles smooth scroll functionality
 * @param {Element[]} links Array of header links
 */
const scrollFunctionality = links => {
  if (!links.length) return;

  links.forEach(link => {
    link.addEventListener('click', bindScrollToLink);
  });
};

/**
 * Handles header squeezing on scroll with intersection observer
 * @param {?Element} header Header dom node
 * @param {?Element} minimizer Header content pusher node
 */
const minimizeHeader = (header, minimizer) => {
  if (!header || !minimizer) return;

  /**
   * When header minimizer isn't visible, squeezes the header
   * @param {Object[]} entries Array of intersecting entries
   */
  const watchMinimizer = entries => {
    entries.forEach(entry => {
      if (entry.isIntersecting) {
        header.classList.remove('minimized');
      } else {
        header.classList.add('minimized');
      }
    });
  };

  const options = {
    root: null,
    rootMargin: '0px',
    threshold: 0
  };

  const observer = new IntersectionObserver(watchMinimizer, options);
  observer.observe(minimizer);
};

/**
 * Initializes header functionality
 */
const headerInit = () => {
  const header = document.querySelector('.site-header');
  const headerMinimizer = document.querySelector('.header-minimizer');
  const links = Array.from(document.querySelectorAll('.header-link'));

  scrollFunctionality(links);
  minimizeHeader(header, headerMinimizer);
};

export default headerInit;
