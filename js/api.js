/**
 * Check if api call was successful and resolve a Promise
 * @param {Promise} promise Promise to handle
 * @param {?function(Object):any} fn Callback function to handle resolved Promise data
 */
export const handlePromise = (promise, fn) => {
  if (typeof promise.then !== 'function') {
    throw new Error('First argument is not a Promise');
  }

  promise
    .then(response => {
      if (response.status !== 200) {
        throw new Error('Something went wrong on api server!');
      } else {
        return response.json();
      }
    })
    .then(response => fn(response))
    .catch(error => {
      console.error(error);
    });
};

/**
 * Fetches data from hacker news api
 * @param {string} endpoint Endpoint name or post id
 * @returns {Promise} Promise object
 */
export const apiRequest = endpoint => {
  return window.fetch(`https://hacker-news.firebaseio.com/v0/${endpoint}.json`);
};

/**
 *
 * @param {string} endpoint Endpoint name or post id
 * @param {?function():any} fn Callback function to handle resolved Promise data
 */
export const fetchAndResolve = (endpoint, fn) => {
  return handlePromise(apiRequest(endpoint), fn);
};
