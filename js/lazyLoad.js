/**
 * Swap `data-src` to `src` and toggle `loading` class
 * @param {Object[]} entries Array of intersecting entries
 * @param {Object} observer IntersectionObserver instance
 */
const watchMedia = (entries, observer) => {
  entries.forEach(entry => {
    const { target, isIntersecting } = entry;

    if (isIntersecting) {
      observer.unobserve(target);
      target.src = target.dataset.src;
      target.classList.add('loaded');
    }
  });
  console.log('media to lazyload - ', entries.length);
};

/**
 * init lazyloading of IMGs and IFRAMEs
 */
const lazyLoadInit = () => {
  const nodes = [...document.querySelectorAll('.lazyload')];

  if (!nodes.length) return;

  const options = {
    root: null,
    rootMargin: '0px',
    threshold: 1
  };
  const observer = new IntersectionObserver(watchMedia, options);

  nodes.forEach(node => observer.observe(node));
};

export default lazyLoadInit;
