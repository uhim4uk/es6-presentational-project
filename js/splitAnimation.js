/**
 * Scale text blocks according to viewport position
 * @param {Object[]} entries Array of intersecting entries
 */

const scaleBlocks = entries => {
  entries.forEach(entry => {
    entry.target.style.setProperty('--scale-ratio', entry.intersectionRatio);
  });
};

/**
 * Handles text blocks scaling animation
 */
const splitInit = () => {
  const textBlocks = [...document.querySelectorAll('.split-wrapper')];

  if (!textBlocks.length) return;

  const options = {
    root: null,
    rootMargin: '0px',
    threshold: [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
  };
  const observer = new IntersectionObserver(scaleBlocks, options);

  textBlocks.forEach(block => observer.observe(block));
};

export default splitInit;
